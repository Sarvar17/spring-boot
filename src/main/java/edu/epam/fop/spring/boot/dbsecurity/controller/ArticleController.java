package edu.epam.fop.spring.boot.dbsecurity.controller;

import edu.epam.fop.spring.boot.dbsecurity.entity.Article;
import edu.epam.fop.spring.boot.dbsecurity.repository.ArticleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/article")
public class ArticleController {
    @Autowired
    private ArticleRepository articleRepository;

    @GetMapping("/{title}")
    public ResponseEntity<String> getArticle(@PathVariable String title) {
        Optional<Article> article = articleRepository.findByTitle(title);
        if (article.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(article.get().getText(), HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<Iterable<Article>> getAllArticles() {
        return new ResponseEntity<>(articleRepository.findAll(), HttpStatus.OK);
    }

    @PostMapping
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<String> createArticle(@RequestBody Article article) {
        if (articleRepository.findByTitle(article.getTitle()).isPresent()) {
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }
        articleRepository.save(article);
        return ResponseEntity.status(HttpStatus.CREATED).body("Article created");
    }

    @PutMapping("/{title}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<String> updateArticle(@PathVariable String title, @RequestBody String text) {
        Optional<Article> article = articleRepository.findByTitle(title);
        if (article.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        article.get().setText(text);
        articleRepository.save(article.get());
        return ResponseEntity.status(HttpStatus.OK).body("Article updated");
    }

    @DeleteMapping("/{title}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Void> deleteArticle(@PathVariable String title) {
        Optional<Article> article = articleRepository.findByTitle(title);
        if (article.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        articleRepository.delete(article.get());
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
